# Notes

A notes app that lets users create notes that get saved locally for users to view them later on. A note consists of title, text and image.

Demo Video : https://www.dropbox.com/s/rgns8sj3f8w4n6i/5-Notes.mp4?dl=0