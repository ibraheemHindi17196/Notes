//
//  ViewController.swift
//  Notes
//
//  Created by Ibraheem Hindi on 8/8/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import CoreData

class Note{
    var title = ""
    var description = ""
    var image:UIImage? = nil
    var has_image = false
}

extension UITableViewCell{
    open override func layoutSubviews() {
        let cell_height = self.frame.size.height

        self.imageView?.frame = CGRect(
            x: 20,
            y: cell_height * 0.2,
            width: cell_height * 0.6,
            height: cell_height * 0.6
        )
        
        self.textLabel?.font = UIFont(name: "Avenir Next", size: 22)
        self.textLabel?.frame = CGRect(
            x: (self.imageView?.frame.maxX)! + 20,
            y: cell_height * 0.2,
            width: self.frame.width - (self.imageView?.frame.maxX)! - 20,
            height: cell_height * 0.6
        )
    }
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var table_view: UITableView!
    var notes_list = [Note]()
    var selected_index = -1

    func loadNotes(){
        notes_list.removeAll()
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let context = app_delegate.persistentContainer.viewContext
        
        let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteEntry")
        fetch_request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetch_request)
            var fetched_note = Note()
            
            for result in results as! [NSManagedObject] {
                if let note_title = result.value(forKey: "note_title") as? String {
                    fetched_note.title = note_title
                }
                
                if let note_desc = result.value(forKey: "note_description") as? String {
                    fetched_note.description = note_desc
                }
                
                if let image_data = result.value(forKey: "note_image") as? Data {
                    fetched_note.image = UIImage(data: image_data)!
                }
                
                if let has_image = result.value(forKey: "has_image") as? Bool {
                    fetched_note.has_image = has_image
                }

                notes_list.append(fetched_note)
                fetched_note = Note()
            }
        }
        catch {
            print("Error")
        }
    }
    
    func clearNotes(){
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let context = app_delegate.persistentContainer.viewContext
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteEntry")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do{
            let _ = try context.execute(request)
        }
        catch{
            print("Error clearing notes")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.dataSource = self
        table_view.delegate = self
        table_view.separatorStyle = .singleLine
        table_view.separatorColor = .black
        table_view.backgroundView = UIImageView(image: UIImage(named: "background"))
        table_view.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadNotes()

        if notes_list.count == 0{
            table_view.isHidden = true
        }
        else{
            table_view.isHidden = false
            table_view.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes_list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let current_note = notes_list[indexPath.row]

        cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
        cell.textLabel?.text = current_note.title
        cell.imageView?.image = current_note.image

        return cell
    }
    
    // Select row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected_index = indexPath.row
        performSegue(withIdentifier: "toNote", sender: self)
    }
    
    // Delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete from core data
            let target_note = notes_list[indexPath.row]
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            let context = app_delegate.persistentContainer.viewContext
            
            let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteEntry")
            fetch_request.returnsObjectsAsFaults = false
            
            do {
                let results = try context.fetch(fetch_request)
                
                for result in results as! [NSManagedObject] {
                    if let note_title = result.value(forKey: "note_title") as? String {
                        if let note_desc = result.value(forKey: "note_description") as? String {
                            if note_title == target_note.title && note_desc == target_note.description{
                                context.delete(result)
                                try context.save()
                                notes_list.remove(at: indexPath.row)
                                
                                if notes_list.count == 0{
                                    table_view.isHidden = true
                                }
                                else{
                                    table_view.isHidden = false
                                    table_view.reloadData()
                                }
                                
                                break
                            }
                        }
                    }
                }
            }
            catch {
                print("Error")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
        
        if segue.identifier == "toNote"{
            let selected_note = notes_list[selected_index]
            let destination = segue.destination as! NoteViewController
            destination.note_title = selected_note.title
            destination.note_desc = selected_note.description
            destination.note_image = selected_note.image!
            destination.has_image = selected_note.has_image
        }
    }

    @IBAction func addNote(_ sender: Any) {
        performSegue(withIdentifier: "toNewNote", sender: self)
    }
    
}











