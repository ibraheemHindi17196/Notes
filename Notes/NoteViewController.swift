//
//  NoteViewController.swift
//  Notes
//
//  Created by Ibraheem Hindi on 8/8/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import CoreData

class NoteViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var desc_label: UILabel!
    @IBOutlet weak var image_view: UIImageView!
    
    var selected_image: UIImage? = nil
    var note_title = ""
    var note_desc = ""
    var note_image = UIImage()
    var has_image = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title_label.text = note_title
        desc_label.text = note_desc
        
        if has_image{
            image_view.image = note_image
        }
        else{
            image_view.image = UIImage(named: "tap-me")
            image_view.isUserInteractionEnabled = true
            let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
            image_view.addGestureRecognizer(tap_recognizer)
        }
    }
    
    func setNoteHasImage(){
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let context = app_delegate.persistentContainer.viewContext
        
        let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteEntry")
        fetch_request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetch_request)
            for result in results as! [NSManagedObject] {
                if let title = result.value(forKey: "note_title") as? String {
                    if let desc = result.value(forKey: "note_description") as? String {
                        if title == note_title && desc == note_desc{
                            let image_data = UIImageJPEGRepresentation(selected_image!, 0.5)
                            result.setValue(image_data, forKey: "note_image")
                            result.setValue(true, forKey: "has_image")
                            try context.save()
                            break
                        }
                    }
                }
            }
        }
        catch {
            print("Error")
        }
    }
    
    @objc func selectImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // On selecting image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        selected_image = info[UIImagePickerControllerEditedImage] as? UIImage
        image_view.image = selected_image
        setNoteHasImage()
        self.dismiss(animated: true, completion: nil)
    }

}
