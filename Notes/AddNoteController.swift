//
//  AddNoteController.swift
//  Notes
//
//  Created by Ibraheem Hindi on 8/8/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import CoreData

class AddNoteController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var title_field: UITextField!
    @IBOutlet weak var description_field: UITextField!
    @IBOutlet weak var tap_to_select: UIImageView!
    
    var selected_image:UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title_field.attributedPlaceholder = NSAttributedString(
            string: title_field.placeholder!,
            attributes: [.foregroundColor: UIColor.black]
        )
        
        description_field.attributedPlaceholder = NSAttributedString(
            string: description_field.placeholder!,
            attributes: [.foregroundColor: UIColor.black]
        )
        
        tap_to_select.isUserInteractionEnabled = true
        let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        tap_to_select.addGestureRecognizer(tap_recognizer)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSuccessAlert(){
        let alert = UIAlertController(
            title: "Success",
            message: "Note saved successfully",
            preferredStyle: .alert
        )

        alert.addAction(UIAlertAction(title: "Ok", style: .cancel) {UIAlertAction in
            self.performSegue(withIdentifier: "toAllNotes", sender: self)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func selectImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // On selecting image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selected_image = info[UIImagePickerControllerEditedImage] as? UIImage
        tap_to_select.image = selected_image
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAllNotes"{
            let destination = segue.destination as! ViewController
            destination.modalTransitionStyle = .crossDissolve
        }
    }
    
    @IBAction func save(_ sender: Any) {
        if title_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please enter a title")
        }
        else if description_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please enter a description")
        }
        else{
            // Save new note
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            let context = app_delegate.persistentContainer.viewContext
            
            let entry = NSEntityDescription.insertNewObject(forEntityName: "NoteEntry", into: context)
            entry.setValue(title_field.text!, forKey: "note_title")
            entry.setValue(description_field.text!, forKey: "note_description")

            if selected_image != nil{
                let image = UIImageJPEGRepresentation(selected_image!, 0.5)
                entry.setValue(image, forKey: "note_image")
                entry.setValue(true, forKey: "has_image")
            }
            else{
                selected_image = UIImage(named: "take_note")
                let image = UIImageJPEGRepresentation(selected_image!, 0.5)
                entry.setValue(image, forKey: "note_image")
                entry.setValue(false, forKey: "has_image")
            }
            
            do {
                try context.save()
                showSuccessAlert()
            }
            catch {
                print("Error")
            }
        }
    }
    
    @IBAction func titleEntered(_ sender: Any) {
        resignFirstResponder()
        description_field.becomeFirstResponder()
    }
    
    @IBAction func descriptionEntered(_ sender: Any) {
        resignFirstResponder()
    }
    

}
