//
//  NoteCell.swift
//  Notes
//
//  Created by Ibraheem Hindi on 8/13/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {
    
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var description_label: UILabel!
    @IBOutlet weak var image_view: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
